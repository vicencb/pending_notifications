# SPDX-License-Identifier: 0BSD

CFLAGS?=-Os -Wall
PIDGIN_CFLAGS!=pkg-config --cflags pidgin
PIDGIN_PLUGINDIR!=pkg-config --variable=plugindir pidgin

all: pending_notifications.so
.PHONY: all

pending_notifications.so: pending_notifications.c
	${CC} pending_notifications.c ${PIDGIN_CFLAGS} ${CFLAGS} -fpic -shared -o pending_notifications.so
clean:; rm -f pending_notifications.so
.PHONY: clean

install: install_system
install_home  : pending_notifications.so; install -Dt ${HOME}/.purple/plugins         pending_notifications.so
install_system: pending_notifications.so; install -Dt "${DESTDIR}${PIDGIN_PLUGINDIR}" pending_notifications.so
.PHONY: install install_home install_system

uninstall: uninstall_system
uninstall_home  :; rm -f ${HOME}/.purple/plugins/pending_notifications.so
uninstall_system:; rm -f ${PIDGIN_PLUGINDIR}/pending_notifications.so
.PHONY: uninstall uninstall_home uninstall_system
