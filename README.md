Pidgin plugin for pending notifications.
========================================

Events from pidgin cause a keyboard LED to flash.
Events include:
 1. New mail notification.
 2. Unread instant messages.

To stop the flashing LED, execute `/usr/bin/kill -s URG pidgin`.
It is suggested to set up a shortcut for this; An example for OpenBox:
```
<keybind key="Scroll_Lock"><action name="Execute"><command>/usr/bin/kill -s URG pidgin</command></action></keybind>
```

Dependencies
------------

This plugin depends on pidgin running in the X Window System.

Install
-------

There is a PKGBUILD provided for ArchLinux.
In general, use `make` to build and `make install` to install the plugin
system-wide or `make install_home` to install in your local pidgin directory.

Usage
-----

Once installed start (or re-start) pidgin. The plugin should appear in
the Plugins window `Ctrl-U`. Just enable it.

With the `Configure Plugin` button you can:
 1. Choose whether the LED should flash on email.
 1. Choose whether the LED should flash on update.
 1. Choose which LED to flash.

Rationale
---------

What is this plugin good for?
If you have long periods of inactivity with your computer, the screen saver
will (if you have it configured this way) power off your display.
So you need to awake it just to see if there are any updates.
With this plugin you only need to look at your keyboard.

For sure you will find more use cases!

License
-------

This software is licensed under the 0BSD license (BSD Zero Clause License).

https://spdx.org/licenses/0BSD.html

Caveats
-------

Unfortunately pidgin sends events also for many other things:
 3. Conversation window gains focus.
 4. Sent instant messages.
 5. ...

The new mail notification event pertains to the 'flash on email' group.
All other events pertain to the 'flash on update' group.
If the LED flashing triggers to often for your liking, you can disable
a group by clearing the associated setting.
