// SPDX-License-Identifier: 0BSD

#define PURPLE_PLUGINS

#include <X11/Xlib.h>
#include <conversation.h>
#include <debug.h>
#include <signal.h>
#include <version.h>

#define PLUGIN_NAME "pending notifications"

// From the XChangeKeyboardControl man page: "No standard interpretation of LEDs is defined."
enum {FLASHING_LED = 3}; // 3 seems to match with the Scroll Lock LED.

#define PREFS_BASE "/plugins/core/" PLUGIN_NAME
static const char *FLASH_LED       = PREFS_BASE "/led";
static const char *FLASH_ON_EMAIL  = PREFS_BASE "/flash_on_email";
static const char *FLASH_ON_UPDATE = PREFS_BASE "/flash_on_update";

// The context for the plugin. Needs to be global because of the signal handler.
struct ctx {
  // The X display owns the LEDs.
  Display *display;
  // Used to store the current state of the LED.
  XKeyboardControl values;
  // Only used in rst_flashing.
  guint timer_handle;
  // Communication channel with the signal handler.
  volatile unsigned flashing;
};
static struct ctx ctx;

// The URG signal is ignored by default, so, this command will not kill pidgin when the plugin is not loaded.
#define CMD "/usr/bin/kill -s URG pidgin"

static void signal_handler(int signum) {
  // Tell the timer to stop on next cycle.
  ctx.flashing = 0;
}

static gboolean led_toggle(void *unused) {
  // Get a copy of volatile data.
  unsigned flashing = ctx.flashing;
  // Toggle LED. Turn it off before stopping.
  ctx.values.led_mode = flashing && !ctx.values.led_mode;
  XChangeKeyboardControl(ctx.display, KBLed | KBLedMode, &ctx.values);
  XFlush(ctx.display);
  if (flashing) {
    // Continue flashing.
    return TRUE;
  }
  XCloseDisplay(ctx.display);
  ctx.display = NULL;
  // Disable timer.
  return FALSE;
}

static void set_flashing(void) {
  if (ctx.flashing) {
    // Flashing in progress.
    return;
  }
  if (!ctx.display) {
    ctx.display = XOpenDisplay(NULL);
    if (!ctx.display) {
      purple_debug_error(PLUGIN_NAME, "Error openning X display.");
      return;
    }
  }
  // Start flashing.
  ctx.flashing = 1;
  ctx.timer_handle = purple_timeout_add_seconds(1, (GSourceFunc)led_toggle, NULL);
}

static void rst_flashing(void) {
  ctx.flashing = 0;
  if (ctx.display) {
    purple_timeout_remove(ctx.timer_handle);
    led_toggle(NULL);
  }
}

// Glue code for purple signals.
static void conversation_updated(PurpleConversation *conv, PurpleConvUpdateType type) {
  if (type == PURPLE_CONV_UPDATE_UNSEEN && purple_prefs_get_bool(FLASH_ON_UPDATE)) {
    set_flashing();
  }
}

static void displaying_email_notification(const char *subject, const char *from, const char *to, const char *url) {
  if (purple_prefs_get_bool(FLASH_ON_EMAIL)) {
    set_flashing();
  }
}

static void displaying_emails_notification(
  const char **subjects, const char **froms, const char **tos, const char **urls, guint count
) {
  if (count && purple_prefs_get_bool(FLASH_ON_EMAIL)) {
    set_flashing();
  }
}

static void new_led(const char *name, PurplePrefType type, gconstpointer val, gpointer data) {
  int led = GPOINTER_TO_INT(val);
  if (led < 1 || led > 32) {
    purple_debug_error(PLUGIN_NAME, "LED %d out of range 1..32.", led);
    return;
  }
  rst_flashing();
  ctx.values.led = led;
}

static gboolean plugin_load(PurplePlugin *plugin) {
  if (sigaction(SIGURG, &(struct sigaction){.sa_handler = signal_handler, .sa_flags = SA_RESTART}, NULL)) {
    return FALSE; // Error.
  }
  purple_prefs_add_none(PREFS_BASE);
  purple_prefs_add_bool(FLASH_ON_EMAIL , TRUE);
  purple_prefs_add_bool(FLASH_ON_UPDATE, FALSE);
  purple_prefs_add_int (FLASH_LED      , FLASHING_LED);
  purple_prefs_connect_callback(purple_prefs_get_handle(), FLASH_LED, new_led, NULL);
  purple_signal_connect(
    purple_notify_get_handle(),
    "displaying-email-notification",
    plugin,
    PURPLE_CALLBACK(displaying_email_notification),
    NULL
  );
  purple_signal_connect(
    purple_notify_get_handle(),
    "displaying-emails-notification",
    plugin,
    PURPLE_CALLBACK(displaying_emails_notification),
    NULL
  );
  purple_signal_connect(
    purple_conversations_get_handle(),
    "conversation-updated",
    plugin,
    PURPLE_CALLBACK(conversation_updated),
    NULL
  );
  return TRUE; // Success.
}

static gboolean plugin_unload(PurplePlugin *plugin) {
  rst_flashing();
  if (sigaction(SIGURG, &(struct sigaction){.sa_handler = SIG_IGN, .sa_flags = SA_RESTART}, NULL)) {
    return FALSE; // Error.
  }
  return TRUE; // Success.
}

static void init_plugin(PurplePlugin *plugin) {
  ctx = (struct ctx){.values = {.led = FLASHING_LED}};
}

static PurplePluginPrefFrame *get_plugin_pref_frame(PurplePlugin *plugin) {
  PurplePluginPrefFrame *frame = purple_plugin_pref_frame_new();
  PurplePluginPref *ppref;

  ppref = purple_plugin_pref_new_with_name_and_label(FLASH_ON_EMAIL, "flash on email");
  purple_plugin_pref_frame_add(frame, ppref);

  ppref = purple_plugin_pref_new_with_name_and_label(FLASH_ON_UPDATE, "flash on update");
  purple_plugin_pref_frame_add(frame, ppref);

  ppref = purple_plugin_pref_new_with_name_and_label(FLASH_LED, "led");
  purple_plugin_pref_set_bounds(ppref, 1, 32);
  purple_plugin_pref_frame_add(frame, ppref);

  return frame;
}

static PurplePluginUiInfo prefs_info = {.get_plugin_pref_frame = get_plugin_pref_frame};

static PurplePluginInfo info = {
  .magic         = PURPLE_PLUGIN_MAGIC,
  .major_version = PURPLE_MAJOR_VERSION,
  .minor_version = PURPLE_MINOR_VERSION,
  .type          = PURPLE_PLUGIN_STANDARD,
  .priority      = PURPLE_PRIORITY_DEFAULT,
  .load          = plugin_load,
  .unload        = plugin_unload,
  .prefs_info    = &prefs_info,
  .id            = "gtk-x11-pending_notifications",
  .name          = PLUGIN_NAME,
  .version       = "0.5",
  .summary       = "Flash a LED on pending notifications",
  .description   =
    "Any event that is expected to reach the user causes a LED to flash.\n"
    "To stop the flashing LED, execute '"CMD"'.\n"
    "It is suggested to set up a shortcut for this; An example for OpenBox:\n"
    "<keybind key=\"Scroll_Lock\"><action name=\"Execute\"><command>"CMD"</command></action></keybind>\n"
};

PURPLE_INIT_PLUGIN(pending_notifications, init_plugin, info);
